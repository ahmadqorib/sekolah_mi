<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

Route::prefix('admin')->namespace('Admin')->group(function(){
    Route::get('login', 'Auth\LoginController@showLogin')->name('login');
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    // berita
    Route::resource('kategori_berita', 'Berita\KategoriBeritaController');
});

// Route::get('/home', 'HomeController@index')->name('home');
