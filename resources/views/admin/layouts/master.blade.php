<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.header')
</head>
<body>

    <nav class="navbar navbar-expand-md navbar-light">
        <button class="navbar-toggler ml-auto mb-2 bg-light" type="button" data-toggle="collapse" data-target="#myNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="myNavbar">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 sidebar fixed-top px-0">
                        <div class="p-3 shadow rounded">
                            <div class="media">
                                <img src="{{ asset('storage/user/user-dummy.png') }}" class="w-25 img-circle mr-3 img-fluid">
                                <div class="media-body text-light">
                                    <span class="mt-0">Ahmad Qorib</span>
                                    <span class="text-muted small">administrator</span>
                                </div>
                            </div>
                        </div>
                        <ul class="navbar-nav flex-column mx-auto">
                            <li class="nav-item">
                                <a href="" class="nav-link text-white p-3 sidebar-current">
                                    <i class="fas fa-home text-light"></i> Dashboard
                                </a>
                            </li>
							<li class="nav-item">
                                <a href="" class="nav-link text-white sidebar-link">
								<i class="fas fa-chalkboard-teacher text-light bg-secondary py-2 p-2 rounded-sm"></i> Profile Sekolah
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link text-white sidebar-link ono-dropdown" data-toggle="collapse" data-target="#berita">
									<i class="far fa-file-alt text-light bg-secondary py-2 p-2 rounded-sm text-center"></i> Berita
									<i class="fas fa-angle-right float-right py-2"></i>
								</a>
								<ul id="berita" class="collapse navbar-nav flex-column ml-3">
									<li class="nav-item">
										<a href="" class="nav-link p-2 text-light">Kategori Berita</a>
									</li>
									<li class="nav-item">
										<a href="" class="nav-link p-2 text-light">Data Berita</a>
									</li>
								</ul>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link text-white sidebar-link">
								<i class="fas fa-chalkboard-teacher text-light bg-secondary py-2 p-2 rounded-sm"></i> Pengumuman
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link text-white sidebar-link">
								<i class="fas fa-chalkboard-teacher text-light bg-secondary py-2 p-2 rounded-sm"></i> Agenda
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link text-white sidebar-link">
								<i class="fas fa-chalkboard-teacher text-light bg-secondary py-2 p-2 rounded-sm"></i> Data Mata Pelajaran
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link text-white sidebar-link">
								<i class="fas fa-chalkboard-teacher text-light bg-secondary py-2 p-2 rounded-sm"></i> Data Guru
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link text-white sidebar-link">
								<i class="fas fa-chalkboard-teacher text-light bg-secondary py-2 p-2 rounded-sm"></i> Data Siswa
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link text-white sidebar-link">
								<i class="fas fa-chalkboard-teacher text-light bg-secondary py-2 p-2 rounded-sm"></i> Data Pengguna
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link text-white sidebar-link ono-dropdown" data-toggle="collapse" data-target="#galleri">
									<i class="far fa-file-alt text-light bg-secondary py-2 p-2 rounded-sm text-center"></i> Galleri
									<i class="fas fa-angle-right float-right py-2"></i>
								</a>
								<ul id="galleri" class="collapse navbar-nav flex-column ml-3">
									<li class="nav-item">
										<a href="" class="nav-link p-2 text-light">Album</a>
									</li>
									<li class="nav-item">
										<a href="" class="nav-link p-2 text-light">Photo</a>
									</li>
								</ul>
                            </li>
							<li class="nav-item">
                                <a href="#" class="nav-link text-white sidebar-link ono-dropdown" data-toggle="collapse" data-target="#pengaturan">
									<i class="far fa-file-alt text-light bg-secondary py-2 p-2 rounded-sm text-center"></i> Pengaturan
									<i class="fas fa-angle-right float-right py-2"></i>
								</a>
								<ul id="pengaturan" class="collapse navbar-nav flex-column ml-3">
									<li class="nav-item">
										<a href="" class="nav-link p-2 text-light">Info Sekolah</a>
									</li>
								</ul>
                            </li>
                        </ul>
                    </div>

                    
                    <!-- end of sidebar -->

                    <div class="col-lg-10 bg-dark ml-auto fixed-top py-2">
						<div class="row">
							<div class="col-md-4">
								<h4 class="text-light text-uppercase mb-0 navbar-brand">Dashboard</h4>
							</div>
							<div class="col-md-8">
								<ul class="navbar-nav float-right">
									<li class="nav-item">
										<a href="" class="nav-link"><i class="fas fa-comments text-muted fa-lg"></i></a>
									</li>
									<li class="nav-item">
										<a href="" class="nav-link"><i class="fas fa-bell text-muted fa-lg"></i></a>
									</li>
									<li class="nav-item">
										<a href="" class="nav-link"><i class="fas fa-sign-out-alt text-danger fa-lg"></i></a>
									</li>
								</ul>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 ml-auto pt-5 ">
                    @yield('content')
                </div>
            </div>
        </div>
    </section>

    @include('admin.layouts.footer')
</body>
</html>