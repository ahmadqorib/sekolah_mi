<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<title>@yield('title')</title>

<style>
    body{
        font-family: 'Montserrat', sans-serif;
        background-color: #eee;
    }

    .sidebar{
        height: 100vh;
        background-color: #2f3542;
    }

    .sidebar ul li{
        font-size: 0.9em;
        margin-top: 2px;
    }

    .sidebar ul li a i{
        color: #ff4757;
        width: 35px;
    }

    .bottom-border{
        border-bottom: 1px groove #eee;
    }

    .sidebar-link{
        transition: all .4s;
        background-color: #222f3e;
    }

    .sidebar-link:hover{
        background-color: #576574;
        transition: translateY(-1px);
    }

    .sidebar-current{
        background-color: #ff4757;
        transition: all .3s;
    }

    .sidebar-current:hover{
        background-color: #f66436;
        transition: translateY(-1px);
    }

    .sidebar-menu-2 li{
        /* background-color: #636e72; */
    }

    
</style>