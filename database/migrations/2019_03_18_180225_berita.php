<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Berita extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berita', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategori')->unsigned();
            $table->string('judul_berita');
            $table->string('alias');
            $table->text('short_description');
            $table->string('image');
            $table->text('description');
            $table->text('tags');
            $table->integer('hits')->default(0)	;
            $table->integer('active')->default(1);
            $table->timestamps();
            $table->foreign('id_kategori')->references('id')->on('kategori_berita')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berita');
    }
}
